/* 046267 Computer Architecture - Winter 2019/20 - HW #3 */
/* Implementation (skeleton)  for the dataflow statistics calculator */

#include "dflow_calc.h"
#include <math.h>
#include <cassert>

class node_data{

public:
    unsigned int latency;
    node_data* right;
    node_data* left;
    bool is_orphan;
    int longest_latency;
    unsigned int index;

    node_data(unsigned int latency, unsigned int index = 0): latency(latency),right(nullptr),left(nullptr),
                                     is_orphan(true), longest_latency(-1), index(index){};
};

/** NOTE: this is not a member function **/
// this function calculates recursively the longest (latency wise) route from a given node.
unsigned int getNodeDepth(node_data* node){

    unsigned int r_lat,l_lat;
    // stop condition
    if(!node) return 0;

    if(!node->right) r_lat = 0;
    else r_lat = node->right->latency;

    if(!node->left) l_lat = 0;
    else l_lat = node->left->latency;

    // if we already calculated we just return the value
    if(node->longest_latency != -1) return (unsigned )node->longest_latency;

    // recursion: latency = latency + max(right->latency, left->latency)
    node->longest_latency = (unsigned int)fmax(
            getNodeDepth(node->right) + r_lat ,
            getNodeDepth(node->left) + l_lat
            );

    return (unsigned )node->longest_latency;

}

class prog_data{

public:
    node_data** nodes_array;
    unsigned int size;
    unsigned int max_latency;

    prog_data(unsigned int num_of_nodes, const InstInfo input_array[], const unsigned int opsLatency[]): size(num_of_nodes), max_latency(0) {

        nodes_array = new node_data*[size];

        int cur_opcode;
        for (unsigned int i=0; i<size; i++) {
            cur_opcode = input_array[i].opcode;
            nodes_array[i] = new node_data(opsLatency[cur_opcode],(unsigned)i);
        }

        // set dependencies between nodes
        calc_dependencies(input_array);

        // setting orphans data
        unset_orphans();

        // calculate program max depth
        calcProgDepth();

    };

    ~prog_data() {

        for (unsigned int i=0; i<size; i++) {
            delete nodes_array[i];
        }

        delete[] nodes_array;
    }

    void calc_dependencies(const InstInfo input_array[]) {

        for (unsigned int cmd_id=1; cmd_id<size; cmd_id++) {

            // find dep for first src
            bool res = false;
            int upper_cmd_id = cmd_id;
            while(upper_cmd_id > 0 && res == false){
                upper_cmd_id--;
                res = input_array[cmd_id].src1Idx == (unsigned)input_array[upper_cmd_id].dstIdx;
            }

            // set parent - the left son will be src1_son
            if (res == true) {
                nodes_array[cmd_id]->left = nodes_array[upper_cmd_id];
            }
            else {
                nodes_array[cmd_id]->left = nullptr;
            }


            // find dep for second src
            res = false;
            upper_cmd_id = cmd_id;
            while(upper_cmd_id > 0 && res == false){
                upper_cmd_id--;
                res = input_array[cmd_id].src2Idx == (unsigned)input_array[upper_cmd_id].dstIdx;
            }

            // set parent - the right son will be src2_son
            if (res == true) {
                nodes_array[cmd_id]->right = nodes_array[upper_cmd_id];
            }
            else {
                nodes_array[cmd_id]->right = nullptr;
            }
        }

    }

    void calcProgDepth(){

        unsigned int tmp_latency;
        for(unsigned int i=0;i<size;i++){
            // for each "orphan" node (with no precedents) - calculate the longest latency.
            // we take the max value from all orphan nodes.
            if(nodes_array[i]->is_orphan){
                tmp_latency = getNodeDepth(nodes_array[i]) + nodes_array[i]->latency ;
                if(tmp_latency > this->max_latency){
                    this->max_latency = tmp_latency;
                }
            }
        }
    }

    // this function sets "is_orphan = false" for nodes which have parents.
    void unset_orphans(){

        for(unsigned int i=0;i<size;i++){
            if(nodes_array[i]->left) nodes_array[i]->left->is_orphan = false;
            if(nodes_array[i]->right) nodes_array[i]->right->is_orphan = false;
        }

    }
};

ProgCtx analyzeProg(const unsigned int opsLatency[], const InstInfo progTrace[], unsigned int numOfInsts) {

    // call the constructor of the prog_data class - it will also calculate dependencies and depths.
    prog_data* prog = new prog_data(numOfInsts, progTrace, opsLatency);

    return prog;

}

void freeProgCtx(ProgCtx ctx) {

    delete (prog_data*)ctx;

}

int getInstDepth(ProgCtx ctx, unsigned int theInst) {

    if(!static_cast<prog_data*>(ctx)->nodes_array[theInst]->right and
       !static_cast<prog_data*>(ctx)->nodes_array[theInst]->left){
        return 0;
    }

    unsigned int size = static_cast<prog_data*>(ctx)->size;
    // check that the given index is in range:
    if(theInst < 0 or theInst > (size-1)) return -1;

    return static_cast<prog_data*>(ctx)->nodes_array[theInst]->longest_latency;
}

int getInstDeps(ProgCtx ctx, unsigned int theInst, int *src1DepInst, int *src2DepInst) {

    unsigned int size = static_cast<prog_data*>(ctx)->size;
    // check that the given index is in range:
    if(theInst < 0 or theInst > (size-1)) return -1;

    int src1,src2;
    // if there is no "son" - return -1
    if(!static_cast<prog_data*>(ctx)->nodes_array[theInst]->left){
        src1 = -1;
    }
    else{ // if son exists - return his index
        src1 = static_cast<prog_data*>(ctx)->nodes_array[theInst]->left->index;
    }
    // if there is no "son" - return -1
    if(!static_cast<prog_data*>(ctx)->nodes_array[theInst]->right){
        src2 = -1;
    }
    else{ // if son exists - return his index
        src2 = static_cast<prog_data*>(ctx)->nodes_array[theInst]->right->index;
    }

    *src1DepInst =  src1;
    *src2DepInst =  src2;

    return 0;
}

int getProgDepth(ProgCtx ctx) {
    return static_cast<prog_data*>(ctx)->max_latency;
}



// our tests
void recursion_test1(){

    node_data* node1 = new node_data(3);
    node_data* node2 = new node_data(4);
    node_data* node3 = new node_data(7);
    node_data* node4 = new node_data(2);
    node_data* node5 = new node_data(1);
    node_data* node6 = new node_data(5);

    node1->left = node2;
    node1->right = node3;

    node2->left = node3;
    node2->right = node4;

    node3->left = node5;
    node3->right = node6;

    node4->left = node5;

    assert(getNodeDepth(node1) == 19);

    node1->longest_latency = -1;
    node2->longest_latency = -1;
    node3->longest_latency = -1;
    node4->longest_latency = -1;
    node5->longest_latency = -1;
    node6->longest_latency = -1;

    node1->latency = 2;
    node2->latency = 1;
    node3->latency = 5;
    node4->latency = 10;
    node5->latency = 3;
    node6->latency = 2;

    assert(getNodeDepth(node1) == 16);

}

void recursion_test2(){

    node_data* node1 = new node_data(3);
    node_data* node2 = new node_data(4);
    node_data* node3 = new node_data(7);
    node_data* node4 = new node_data(2);
    node_data* node5 = new node_data(1);
    node_data* node6 = new node_data(5);

    node1->left = node2;
    node1->right = node3;

    node2->left = node3;
    node2->right = node4;

    node3->left = node5;
    node3->right = node6;

    node4->left = node5;
    node4->right = node3;

    assert(getNodeDepth(node1) == 21);

    node1->longest_latency = -1;
    node2->longest_latency = -1;
    node3->longest_latency = -1;
    node4->longest_latency = -1;
    node5->longest_latency = -1;
    node6->longest_latency = -1;

    node1->latency = 2;
    node2->latency = 1;
    node3->latency = 5;
    node4->latency = 10;
    node5->latency = 3;
    node6->latency = 2;

    assert(getNodeDepth(node1) == 21);

}

void test_create_prog_data() {

    // opsLatency[0] = 0, so it starts from the first index

    unsigned int num_of_insts = 4;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {1,0,0,0};
    InstInfo inst_1 {2,0,0,0};
    InstInfo inst_2 {3,0,0,0};
    InstInfo inst_3 {1,0,0,0};
    InstInfo progTrace[4] = {inst_0, inst_1,inst_2,inst_3};
    unsigned int opsLatency[4] = {0,5,4,7};


    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.size == num_of_insts);
    assert(prog.nodes_array[0]->latency == 5);
    assert(prog.nodes_array[1]->latency == 4);
    assert(prog.nodes_array[2]->latency == 7);
    assert(prog.nodes_array[3]->latency == 5);

}

void test_calc_prog_depth(){

    InstInfo inst_1 {1,0,0,0};
    InstInfo inst_2 {2,0,0,0};
    InstInfo inst_3 {3,0,0,0};
    InstInfo inst_4 {4,0,0,0};
    InstInfo inst_5 {5,0,0,0};
    InstInfo inst_6 {6,0,0,0};
    InstInfo inst_7 {7,0,0,0};
    InstInfo inst_8 {8,0,0,0};
    InstInfo inst_9 {9,0,0,0};
    InstInfo inst_10 {10,0,0,0};
    InstInfo inst_11 {11,0,0,0};

    InstInfo progTrace[11] = {inst_1, inst_2,inst_3,inst_4,inst_5
            ,inst_6,inst_7,inst_8,inst_9,inst_10,inst_11};

    unsigned int opsLatency[12] = {0,3,4,7,2,1,5,3,1,3,2,1};

    prog_data prog(11,progTrace,opsLatency);

    prog.nodes_array[0]->right = prog.nodes_array[1];
    prog.nodes_array[0]->left = nullptr;

    prog.nodes_array[1]->right = prog.nodes_array[2];
    prog.nodes_array[1]->left = nullptr;

    prog.nodes_array[2]->right = nullptr;
    prog.nodes_array[2]->left = nullptr;

    prog.nodes_array[3]->right = prog.nodes_array[7];
    prog.nodes_array[3]->left = prog.nodes_array[2];

    prog.nodes_array[4]->left = prog.nodes_array[3];
    prog.nodes_array[4]->right = nullptr;

    prog.nodes_array[5]->right = prog.nodes_array[8];
    prog.nodes_array[5]->left = prog.nodes_array[6];

    prog.nodes_array[6]->right = prog.nodes_array[7];
    prog.nodes_array[6]->left = nullptr;

    prog.nodes_array[7]->right = nullptr;
    prog.nodes_array[7]->left = nullptr;

    prog.nodes_array[8]->right = prog.nodes_array[9];
    prog.nodes_array[8]->left = nullptr;

    prog.nodes_array[9]->right = prog.nodes_array[10];
    prog.nodes_array[9]->left = nullptr;

    prog.nodes_array[10]->right = nullptr;
    prog.nodes_array[10]->left = nullptr;

    prog.nodes_array[0]->is_orphan = true;
    prog.nodes_array[1]->is_orphan = false;
    prog.nodes_array[2]->is_orphan = false;
    prog.nodes_array[3]->is_orphan = false;
    prog.nodes_array[4]->is_orphan = true;
    prog.nodes_array[5]->is_orphan = true;
    prog.nodes_array[6]->is_orphan = false;
    prog.nodes_array[7]->is_orphan = false;
    prog.nodes_array[8]->is_orphan = false;
    prog.nodes_array[9]->is_orphan = false;
    prog.nodes_array[10]->is_orphan = false;

    prog.calcProgDepth();

    assert(getNodeDepth(prog.nodes_array[4]) == 10);
    assert(getNodeDepth(prog.nodes_array[5]) == 11);
    assert(prog.max_latency == 14);

}


void test_calc_dependencies_0() {  // no dep case

    unsigned int opcode = 1;
    unsigned int num_of_insts = 2;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {opcode,4,0,0};
    InstInfo inst_1 {opcode,3,0,0};
    InstInfo progTrace[2] = {inst_0, inst_1};
    unsigned int opsLatency[2] = {0,5}; // opsLatency[0] = 0, so it starts from the first index


    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.nodes_array[0]->right == nullptr);
    assert(prog.nodes_array[0]->left == nullptr);
    assert(prog.nodes_array[1]->right == nullptr);
    assert(prog.nodes_array[1]->left == nullptr);

}

void test_calc_dependencies_1() {  // easiest case
                                   // src 1 only
                                   // only 1 dependency
    unsigned int opcode = 1;
    unsigned int num_of_insts = 2;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {opcode,2,1,1};
    InstInfo inst_1 {opcode,3,2,0};
    InstInfo progTrace[2] = {inst_0, inst_1};
    unsigned int opsLatency[2] = {0,5}; // opsLatency[0] = 0, so it starts from the first index


    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.nodes_array[0]->right == nullptr);
    assert(prog.nodes_array[0]->left == nullptr);
    assert(prog.nodes_array[1]->right == prog.nodes_array[0]);
    assert(prog.nodes_array[1]->left == nullptr);

}

void test_calc_dependencies_2() {  // gap between dst's cmd and src's cmd
                                   // src 1 only
                                   // only 1 dependency
                                   // dst appears a couple of times

    unsigned int opcode = 1;
    unsigned int num_of_insts = 4;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {opcode,2,0,0};
    InstInfo inst_1 {opcode,2,0,0};
    InstInfo inst_2 {opcode,1,0,0};
    InstInfo inst_3 {opcode,1,2,0};
    InstInfo progTrace[4] = {inst_0, inst_1, inst_2, inst_3};
    unsigned int opsLatency[2] = {0,5};  // opsLatency[0] = 0, so it starts from the first index


    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.nodes_array[0]->right == nullptr);
    assert(prog.nodes_array[0]->left == nullptr);
    assert(prog.nodes_array[1]->right == nullptr);
    assert(prog.nodes_array[1]->left == nullptr);
    assert(prog.nodes_array[2]->right == nullptr);
    assert(prog.nodes_array[2]->left == nullptr);
    assert(prog.nodes_array[3]->right == prog.nodes_array[1]);
    assert(prog.nodes_array[3]->left == nullptr);

}

void test_calc_dependencies_3() {  // gap between dst's cmd and src's cmd
                                   // src 1 only
                                   // multiple dependencies of different cmds (each dst has only 1 src depending on him)
                                   // dst appears a couple of times

    unsigned int opcode = 1;
    unsigned int num_of_insts = 4;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {opcode,2,0,0};
    InstInfo inst_1 {opcode,3,0,0};
    InstInfo inst_2 {opcode,1,3,0};
    InstInfo inst_3 {opcode,1,2,0};
    InstInfo progTrace[4] = {inst_0, inst_1, inst_2, inst_3};
    unsigned int opsLatency[2] = {0,5};  // opsLatency[0] = 0, so it starts from the first index


    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.nodes_array[0]->right == nullptr);
    assert(prog.nodes_array[0]->left == nullptr);
    assert(prog.nodes_array[1]->right == nullptr);
    assert(prog.nodes_array[1]->left == nullptr);
    assert(prog.nodes_array[2]->right == prog.nodes_array[1]);
    assert(prog.nodes_array[2]->left == nullptr);
    assert(prog.nodes_array[3]->right == prog.nodes_array[0]);
    assert(prog.nodes_array[3]->left == nullptr);

}

void test_calc_dependencies_4() {  // gap between dst's cmd and src's cmd
                                   // src 1 only
                                   // multiple dependencies of same cmds (dst has multiple src depending on him)
                                   // dst appears a couple of times

    unsigned int opcode = 1;
    unsigned int num_of_insts = 4;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {opcode,2,0,0};
    InstInfo inst_1 {opcode,3,3,0};
    InstInfo inst_2 {opcode,1,3,0};
    InstInfo inst_3 {opcode,1,3,0};
    InstInfo progTrace[4] = {inst_0, inst_1, inst_2, inst_3};
    unsigned int opsLatency[2] = {0,5};  // opsLatency[0] = 0, so it starts from the first index


    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.nodes_array[0]->right == nullptr);
    assert(prog.nodes_array[0]->left == nullptr);
    assert(prog.nodes_array[1]->right == nullptr);
    assert(prog.nodes_array[1]->left == nullptr);
    assert(prog.nodes_array[2]->right == prog.nodes_array[1]);
    assert(prog.nodes_array[2]->left == nullptr);
    assert(prog.nodes_array[3]->right == prog.nodes_array[1]);
    assert(prog.nodes_array[3]->left == nullptr);

}

void test_calc_dependencies_5() {   // easiest case
                                    // src 2 only
                                    // only 1 dependency
    unsigned int opcode = 1;
    unsigned int num_of_insts = 2;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {opcode,2,1,1};
    InstInfo inst_1 {opcode,3,0,2};
    InstInfo progTrace[2] = {inst_0, inst_1};
    unsigned int opsLatency[2] = {0,5}; // opsLatency[0] = 0, so it starts from the first index


    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.nodes_array[0]->right == nullptr);
    assert(prog.nodes_array[0]->left == nullptr);
    assert(prog.nodes_array[1]->right == nullptr);
    assert(prog.nodes_array[1]->left == prog.nodes_array[0]);

}

void test_calc_dependencies_6() {   // gap between dst's cmd and src's cmd
                                    // src 2 only
                                    // only 1 dependency
                                    // dst appears a couple of times

    unsigned int opcode = 1;
    unsigned int num_of_insts = 4;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {opcode,2,0,0};
    InstInfo inst_1 {opcode,2,0,0};
    InstInfo inst_2 {opcode,1,0,0};
    InstInfo inst_3 {opcode,1,0,2};
    InstInfo progTrace[4] = {inst_0, inst_1, inst_2, inst_3};
    unsigned int opsLatency[2] = {0,5};  // opsLatency[0] = 0, so it starts from the first index


    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.nodes_array[0]->right == nullptr);
    assert(prog.nodes_array[0]->left == nullptr);
    assert(prog.nodes_array[1]->right == nullptr);
    assert(prog.nodes_array[1]->left == nullptr);
    assert(prog.nodes_array[2]->right == nullptr);
    assert(prog.nodes_array[2]->left == nullptr);
    assert(prog.nodes_array[3]->right == nullptr);
    assert(prog.nodes_array[3]->left == prog.nodes_array[1]);

}

void test_calc_dependencies_7() {   // gap between dst's cmd and src's cmd
                                    // src 2 only
                                    // multiple dependencies of different cmds (each dst has only 1 src depending on him)
                                    // dst appears a couple of times

    unsigned int opcode = 1;
    unsigned int num_of_insts = 4;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {opcode,2,0,0};
    InstInfo inst_1 {opcode,3,0,0};
    InstInfo inst_2 {opcode,1,0,3};
    InstInfo inst_3 {opcode,1,0,2};
    InstInfo progTrace[4] = {inst_0, inst_1, inst_2, inst_3};
    unsigned int opsLatency[2] = {0,5};  // opsLatency[0] = 0, so it starts from the first index


    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.nodes_array[0]->right == nullptr);
    assert(prog.nodes_array[0]->left == nullptr);
    assert(prog.nodes_array[1]->right == nullptr);
    assert(prog.nodes_array[1]->left == nullptr);
    assert(prog.nodes_array[2]->right == nullptr);
    assert(prog.nodes_array[2]->left == prog.nodes_array[1]);
    assert(prog.nodes_array[3]->right == nullptr);
    assert(prog.nodes_array[3]->left == prog.nodes_array[0]);

}

void test_calc_dependencies_8() {   // gap between dst's cmd and src's cmd
                                    // src 2 only
                                    // multiple dependencies of same cmds (dst has multiple src depending on him)
                                    // dst appears a couple of times

    unsigned int opcode = 1;
    unsigned int num_of_insts = 4;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {opcode,2,0,0};
    InstInfo inst_1 {opcode,3,0,3};
    InstInfo inst_2 {opcode,1,0,3};
    InstInfo inst_3 {opcode,1,0,3};
    InstInfo progTrace[4] = {inst_0, inst_1, inst_2, inst_3};
    unsigned int opsLatency[2] = {0,5};  // opsLatency[0] = 0, so it starts from the first index


    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.nodes_array[0]->right == nullptr);
    assert(prog.nodes_array[0]->left == nullptr);
    assert(prog.nodes_array[1]->right == nullptr);
    assert(prog.nodes_array[1]->left == nullptr);
    assert(prog.nodes_array[2]->right == nullptr);
    assert(prog.nodes_array[2]->left == prog.nodes_array[1]);
    assert(prog.nodes_array[3]->right == nullptr);
    assert(prog.nodes_array[3]->left == prog.nodes_array[1]);

}

void test_calc_dependencies_9() {   // easiest case
                                    // src 1 and 2
                                    // only 1 dependency
    unsigned int opcode = 1;
    unsigned int num_of_insts = 2;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {opcode,2,1,1};
    InstInfo inst_1 {opcode,3,2,2};
    InstInfo progTrace[2] = {inst_0, inst_1};
    unsigned int opsLatency[2] = {0,5}; // opsLatency[0] = 0, so it starts from the first index


    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.nodes_array[0]->right == nullptr);
    assert(prog.nodes_array[0]->left == nullptr);
    assert(prog.nodes_array[1]->right == prog.nodes_array[0]);
    assert(prog.nodes_array[1]->left == prog.nodes_array[0]);

}

void test_calc_dependencies_10() {   // gap between dst's cmd and src's cmd
                                     // src 1 and src 2
                                     // different deps for src 1 and src 2

    unsigned int opcode = 1;
    unsigned int num_of_insts = 3;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {opcode,2,0,0};
    InstInfo inst_1 {opcode,3,0,0};
    InstInfo inst_2 {opcode,1,3,2};
    InstInfo progTrace[3] = {inst_0, inst_1, inst_2};
    unsigned int opsLatency[2] = {0,5};  // opsLatency[0] = 0, so it starts from the first index

    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.nodes_array[0]->right == nullptr);
    assert(prog.nodes_array[0]->left == nullptr);
    assert(prog.nodes_array[1]->right == nullptr);
    assert(prog.nodes_array[1]->left == nullptr);
    assert(prog.nodes_array[2]->right == prog.nodes_array[1]);
    assert(prog.nodes_array[2]->left == prog.nodes_array[0]);
}

void test_calc_dependencies_11() {   // gap between dst's cmd and src's cmd
                                     // src 1 and src 2
                                     // multiple dependencies of different cmds

    unsigned int opcode = 1;
    unsigned int num_of_insts = 4;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {opcode,2,0,0};
    InstInfo inst_1 {opcode,3,0,0};
    InstInfo inst_2 {opcode,1,3,2};
    InstInfo inst_3 {opcode,1,2,3};
    InstInfo progTrace[4] = {inst_0, inst_1, inst_2, inst_3};
    unsigned int opsLatency[2] = {0,5};  // opsLatency[0] = 0, so it starts from the first index


    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.nodes_array[0]->right == nullptr);
    assert(prog.nodes_array[0]->left == nullptr);
    assert(prog.nodes_array[1]->right == nullptr);
    assert(prog.nodes_array[1]->left == nullptr);
    assert(prog.nodes_array[2]->right == prog.nodes_array[1]);
    assert(prog.nodes_array[2]->left == prog.nodes_array[0]);
    assert(prog.nodes_array[3]->right == prog.nodes_array[0]);
    assert(prog.nodes_array[3]->left == prog.nodes_array[1]);

}

void test_calc_dependencies_12() {   // gap between dst's cmd and src's cmd
                                     // src 1 and src 2
                                     // multiple dependencies of different cmds

    unsigned int opcode = 1;
    unsigned int num_of_insts = 4;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {opcode,2,0,0};
    InstInfo inst_1 {opcode,3,0,0};
    InstInfo inst_2 {opcode,1,2,2};
    InstInfo inst_3 {opcode,1,2,2};
    InstInfo progTrace[4] = {inst_0, inst_1, inst_2, inst_3};
    unsigned int opsLatency[2] = {0,5};  // opsLatency[0] = 0, so it starts from the first index


    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.nodes_array[0]->right == nullptr);
    assert(prog.nodes_array[0]->left == nullptr);
    assert(prog.nodes_array[1]->right == nullptr);
    assert(prog.nodes_array[1]->left == nullptr);
    assert(prog.nodes_array[2]->right == prog.nodes_array[0]);
    assert(prog.nodes_array[2]->left == prog.nodes_array[0]);
    assert(prog.nodes_array[3]->right == prog.nodes_array[0]);
    assert(prog.nodes_array[3]->left == prog.nodes_array[0]);

}

void test_calc_dependencies_13() {   // gap between dst's cmd and src's cmd
                                     // src 1 and src 2
                                     // multiple dependencies of different cmds
                                     // dst appears multiple times

    unsigned int opcode = 1;
    unsigned int num_of_insts = 6;
    // <opcode> <dst> <src1> <src2>
    InstInfo inst_0 {opcode,2,0,0};
    InstInfo inst_1 {opcode,3,0,0};
    InstInfo inst_2 {opcode,2,0,0};
    InstInfo inst_3 {opcode,3,0,0};
    InstInfo inst_4 {opcode,1,3,2};
    InstInfo inst_5 {opcode,1,2,3};
    InstInfo progTrace[6] = {inst_0, inst_1, inst_2, inst_3, inst_4, inst_5};
    unsigned int opsLatency[2] = {0,5};  // opsLatency[0] = 0, so it starts from the first index


    prog_data prog = prog_data(num_of_insts, progTrace, opsLatency);
    assert(prog.nodes_array[0]->right == nullptr);
    assert(prog.nodes_array[0]->left == nullptr);
    assert(prog.nodes_array[1]->right == nullptr);
    assert(prog.nodes_array[1]->left == nullptr);
    assert(prog.nodes_array[2]->right == nullptr);
    assert(prog.nodes_array[2]->left == nullptr);
    assert(prog.nodes_array[3]->right == nullptr);
    assert(prog.nodes_array[3]->left == nullptr);
    assert(prog.nodes_array[4]->right == prog.nodes_array[3]);
    assert(prog.nodes_array[4]->left == prog.nodes_array[2]);
    assert(prog.nodes_array[5]->right == prog.nodes_array[2]);
    assert(prog.nodes_array[5]->left == prog.nodes_array[3]);

}

void run_tests(){

    recursion_test1();
    recursion_test2();
    test_create_prog_data();
    //test_calc_prog_depth();
    test_calc_dependencies_0();
    test_calc_dependencies_1();
    test_calc_dependencies_2();
    test_calc_dependencies_3();
    test_calc_dependencies_4();
    test_calc_dependencies_5();
    test_calc_dependencies_6();
    test_calc_dependencies_7();
    test_calc_dependencies_8();
    test_calc_dependencies_9();
    test_calc_dependencies_10();
    test_calc_dependencies_11();
    test_calc_dependencies_12();
    test_calc_dependencies_13();
};


